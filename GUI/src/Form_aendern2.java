import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Form_aendern2 extends JFrame {

	private JPanel pnlHintergrund;
	private JTextField tfdtextfeld;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Form_aendern2 frame = new Form_aendern2();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Form_aendern2() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 394, 738);
		pnlHintergrund = new JPanel();
		pnlHintergrund.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(pnlHintergrund);
		pnlHintergrund.setLayout(null);

		JLabel lblUeberschrift = new JLabel("Dieser Text soll ver\u00E4ndert werden.");
		lblUeberschrift.setName("lblUeberschrift");
		lblUeberschrift.setBounds(0, 49, 376, 53);
		pnlHintergrund.add(lblUeberschrift);

		JLabel lblText1 = new JLabel("Aufgabe 1: Hintergrundfarbe \u00E4ndern");
		lblText1.setName("lblText1");
		lblText1.setBounds(26, 156, 362, 29);
		pnlHintergrund.add(lblText1);

		JButton btnrot = new JButton("Rot");
		btnrot.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				pnlHintergrund.setBackground(Color.RED);
			}
		});
		btnrot.setBounds(26, 198, 97, 25);
		pnlHintergrund.add(btnrot);

		JButton btngruen = new JButton("Gr\u00FCn");
		btngruen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				pnlHintergrund.setBackground(Color.GREEN);
			}
		});
		btngruen.setBounds(135, 198, 97, 25);
		pnlHintergrund.add(btngruen);

		JButton btnblau = new JButton("Blau");
		btnblau.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				pnlHintergrund.setBackground(Color.BLUE);
			}
		});
		btnblau.setBounds(248, 198, 97, 25);
		pnlHintergrund.add(btnblau);

		JButton btngelb = new JButton("Gelb");
		btngelb.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				pnlHintergrund.setBackground(Color.YELLOW);
			}
		});
		btngelb.setBounds(12, 236, 97, 25);
		pnlHintergrund.add(btngelb);

		JButton btnstandard = new JButton("Standardfarbe");
		btnstandard.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				pnlHintergrund.setBackground(new Color(15658734));
			}
		});
		btnstandard.setBounds(121, 236, 121, 25);
		pnlHintergrund.add(btnstandard);

		JButton btnwaehlen = new JButton("Farbe w\u00E4hlen");
		btnwaehlen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				pnlHintergrund.setBackground(chooseColor());
			}
		});
		btnwaehlen.setBounds(248, 236, 119, 25);
		pnlHintergrund.add(btnwaehlen);

		JButton btnarial = new JButton("Arial");
		btnarial.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				lblUeberschrift.setFont(new Font("Arial", 0, lblUeberschrift.getFont().getSize()));
			}
		});
		btnarial.setBounds(12, 290, 97, 25);
		pnlHintergrund.add(btnarial);

		JButton btncomic = new JButton("Comic Sans MS");
		btncomic.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				lblUeberschrift.setFont(new Font("Comic Sans MS", 0, lblUeberschrift.getFont().getSize()));
			}
		});
		btncomic.setBounds(121, 290, 135, 25);
		pnlHintergrund.add(btncomic);

		JButton btncourier = new JButton("Courier New");
		btncourier.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				lblUeberschrift.setFont(new Font("Courier New", 0, lblUeberschrift.getFont().getSize()));
			}
		});
		btncourier.setBounds(257, 290, 110, 25);
		pnlHintergrund.add(btncourier);

		JButton btnschreiben = new JButton("Ins Label schreiben");
		btnschreiben.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				lblUeberschrift.setText(tfdtextfeld.getText());
			}
		});
		btnschreiben.setBounds(26, 363, 160, 25);
		pnlHintergrund.add(btnschreiben);

		JButton btnloeschen = new JButton("Text im Label l\u00F6schen");
		btnloeschen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				lblUeberschrift.setText("");
			}
		});
		btnloeschen.setBounds(198, 363, 160, 25);
		pnlHintergrund.add(btnloeschen);

		JButton btnrotschrift = new JButton("Rot");
		btnrotschrift.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				lblUeberschrift.setForeground(Color.RED);
			}
		});
		btnrotschrift.setBounds(26, 442, 97, 25);
		pnlHintergrund.add(btnrotschrift);

		JButton btnblauschrift = new JButton("Blau");
		btnblauschrift.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				lblUeberschrift.setForeground(Color.BLUE);
			}
		});
		btnblauschrift.setBounds(135, 442, 97, 25);
		pnlHintergrund.add(btnblauschrift);

		JButton btnschwarzschrift = new JButton("Schwarz");
		btnschwarzschrift.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				lblUeberschrift.setForeground(Color.BLACK);
			}
		});
		btnschwarzschrift.setBounds(248, 442, 97, 25);
		pnlHintergrund.add(btnschwarzschrift);

		JButton btnplus = new JButton("+");
		btnplus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String standard = lblUeberschrift.getFont().getFontName();
				int x = lblUeberschrift.getFont().getSize();
				lblUeberschrift.setFont(new Font(standard, 0, x + 1));
			}
		});
		btnplus.setBounds(57, 514, 97, 25);
		pnlHintergrund.add(btnplus);

		JButton btnminus = new JButton("-");
		btnminus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String standard = lblUeberschrift.getFont().getFontName();
				int x = lblUeberschrift.getFont().getSize();
				if (x > 1)
					x--;
				lblUeberschrift.setFont(new Font(standard, 0, x));
			}
		});
		btnminus.setBounds(198, 514, 97, 25);
		pnlHintergrund.add(btnminus);

		JButton btnlinks = new JButton("linksb\u00FCndig");
		btnlinks.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				lblUeberschrift.setHorizontalAlignment(2);
			}
		});
		btnlinks.setBounds(26, 594, 97, 25);
		pnlHintergrund.add(btnlinks);

		JButton btnzentriert = new JButton("zentriert");
		btnzentriert.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				lblUeberschrift.setHorizontalAlignment(0);
			}
		});
		btnzentriert.setBounds(135, 594, 97, 25);
		pnlHintergrund.add(btnzentriert);

		JButton btnrechts = new JButton("rechtsb\u00FCndig");
		btnrechts.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				lblUeberschrift.setHorizontalAlignment(4);
			}
		});
		btnrechts.setBounds(248, 594, 119, 25);
		pnlHintergrund.add(btnrechts);

		JButton btnexit = new JButton("EXIT");
		btnexit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				System.exit(1);
			}
		});
		btnexit.setBounds(135, 662, 97, 25);
		pnlHintergrund.add(btnexit);

		JLabel lblschriftfarbe = new JLabel("Aufgabe 3: Schriftfarbe \u00E4ndern");
		lblschriftfarbe.setBounds(25, 401, 362, 29);
		pnlHintergrund.add(lblschriftfarbe);

		JLabel lblschriftgroesse = new JLabel("Schriftgr\u00F6\u00DFe ver\u00E4ndern");
		lblschriftgroesse.setBounds(26, 480, 362, 29);
		pnlHintergrund.add(lblschriftgroesse);

		JLabel lblausrichtung = new JLabel("Aufgabe 5: Textausrichtung");
		lblausrichtung.setBounds(26, 552, 362, 29);
		pnlHintergrund.add(lblausrichtung);

		JLabel lblbeenden = new JLabel("Aufgabe 6: Programm beenden");
		lblbeenden.setBounds(25, 620, 362, 29);
		pnlHintergrund.add(lblbeenden);

		tfdtextfeld = new JTextField();
		tfdtextfeld.setText("Hier bitte Text eingeben");
		tfdtextfeld.setBounds(26, 328, 315, 29);
		pnlHintergrund.add(tfdtextfeld);
		tfdtextfeld.setColumns(10);

		JLabel lblformatieren = new JLabel("Aufgabe 2: Text formatieren");
		lblformatieren.setName("lblText1");
		lblformatieren.setBounds(25, 261, 362, 29);
		pnlHintergrund.add(lblformatieren);

	}

	public Color chooseColor() {
		return JColorChooser.showDialog(this, "WFarbe", Color.white);
	}
}
